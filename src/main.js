// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from '@/App'

// Router
import router from '@/config/router.js'

// Vuex
import { store } from '@/config/store.js'

// Firebase
import firebase from 'firebase'
import firebaseui from 'firebaseui'
import { config } from '@/config/firebase'

// Forms validation
import VeeValidate from 'vee-validate'
import { Validator } from 'vee-validate'
import messagesEs from '@/config/validation/messages/es.js'
import attributesEs from '@/config/validation/attributes/es.js'
import messagesEn from '@/config/validation/messages/en.js'
import attributesEn from '@/config/validation/attributes/en.js'

// Animate.css library
import 'animate.css/animate.min.css'

Vue.use(VeeValidate, {
    locale: 'es',
    dictionary: {
        es: {
            messages: messagesEs,
            attributes: attributesEs
        },
        en: {
            messages: messagesEn,
            attributes: attributesEn
        }
    }
});

// Validator.extend('age', {
//     getMessage: (field, [args]) => `Tienes que ser mayor de ${args} años`,
//     validate: (value, [args]) => value >= args
// });

Vue.config.productionTip = false

// Event Bus
export const bus = new Vue();

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: { App },
    created() {
        firebase.initializeApp(config);
        firebase.auth().onAuthStateChanged((user) => {
            if(!user) {
                this.$router.push('/auth')
            } else if(user && this.$route.path == '/auth') {
                this.$router.push('/')
            } else {
                this.$router.push(this.$route.path)
            }
        });
    }
})
