import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// Spa
import authPage from '@/spa/auth/auth'
import homePage from '@/spa/home/home'
import resultsPage from '@/spa/results/results'
import summaryPage from '@/spa/summary/summary'
import paxPage from '@/spa/pax/pax'
import extrasPage from '@/spa/extras/extras'
import comboPage from '@/spa/combo/combo'
import payPage from '@/spa/pay'
import confirmPage from '@/spa/confirmation'

const routes = [
    {
        path: '/auth',
        name: 'auth-page',
        component: authPage,
        meta: { title: 'eDreams - Login' }
    },
    {
        path: '/',
        name: 'home-page',
        component: homePage,
        meta: { title: 'eDreams' }
    },
    {
        path: '/results',
        name: 'results-page',
        component: resultsPage,
        meta: { title: 'eDreams - Resultados' }
    },
    {
        path: '/summary',
        name: 'summary-page',
        component: summaryPage,
        meta: { title: 'eDreams - Tu vuelo' }
    },
    {
        path: '/pax/:state',
        name: 'pax-page',
        component: paxPage,
        meta: { title: 'eDreams - Pasajeros' }
    },
    {
        path: '/extras',
        name: 'extras-page',
        component: extrasPage,
        meta: { title: 'eDreams - Extras' }
    },
    {
        path: '/combo/:state',
        name: 'combo-page',
        component: comboPage,
        meta: { title: 'eDreams - Personaliza tu viaje' }
    },
    {
        path: '/pay',
        name: 'pay-page',
        component: payPage,
        meta: { title: 'eDreams - Pago' }
    },
    {
        path: '/confirm',
        name: 'confirm',
        component: confirmPage,
        meta: { title: 'eDreams - Confirmación' }
    }
]

const router = new Router({
    routes,
    scrollBehavior(to, from, savedPosition) {
        return { x: 0, y: 0 }
    },
    mode: 'history'
})

router.beforeEach((to, from, next) => {
    document.title = to.meta.title
    next()
});

export default router
