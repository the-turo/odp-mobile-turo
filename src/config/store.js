import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        loader: false,
        flightAmount: 210.95,
        managementCost: 40.00,
        discount: 40.00,
        totalAmount: 0,
        ancillaries: [
            {
                name: 'baggage',
                img: '../static/images/ancillaries/baggage.png',
                title: 'Equipaje facturado',
                type: 'protect',
                grid: 12,
                text: 'Ahorra comprando online. Facturar maletas de puede costar hasta un 50% más en el aeropuerto',
                price: 15.99,
                added: false,
                options: {
                    available: false,
                    display: false
                },
                views: {
                    pax: true,
                    extra: false
                },
                discount: {
                    available: true,
                    amount: 50
                }
            },
            {
                name: 'seats',
                img: '../static/images/ancillaries/seats.png',
                title: 'Escoge tus asientos',
                type: 'protect',
                grid: 12,
                text: 'Siéntate con tu familia y amigos o disfruta de más espacio para tus piernas',
                price: 3.99,
                added: false,
                options: {
                    available: false,
                    display: false
                },
                views: {
                    pax: true,
                    extra: false
                },
                discount: {
                    available: false,
                    amount: 50
                }
            },
            {
                name: 'insurance',
                img: './static/images/ancillaries/travel.png',
                title: 'Seguro de viaje',
                type: 'protect',
                grid: 12,
                text: 'Viaja tranquilo y sin preocupaciones con nuestro seguo médico y de cancelación',
                price: 25.99,
                added: false,
                options: {
                    available: true,
                    display: false
                },
                views: {
                    pax: false,
                    extra: true
                },
                discount: {
                    available: false,
                    amount: 50
                }
            },
            {
                name: 'delay',
                img: './static/images/ancillaries/delay.png',
                title: 'Compensación por retraso',
                type: 'protect',
                grid: 6,
                text: '¿Y si tu vuelo se retrasa? Recibirás un cupón de hasta 100€ si tu vuelo se retrasa una hora o más!',
                price: 10.00,
                added: false,
                options: {
                    available: false,
                    display: false
                },
                views: {
                    pax: false,
                    extra: true
                },
                discount: {
                    available: false,
                    amount: 50
                }
            },
            {
                name: 'baggage-insurance',
                img: './static/images/ancillaries/baggage.png',
                title: 'Seguro de maletas',
                type: 'protect',
                grid: 6,
                text: 'Sigue protegiendo tus maletas facturadas y de mano aún cuando no las tienes a la vista',
                price: 5.00,
                added: false,
                options: {
                    available: false,
                    display: false
                },
                views: {
                    pax: false,
                    extra: true
                },
                discount: {
                    available: false,
                    amount: 50
                }
            },
            {
                name: 'flex-pax',
                img: './static/images/ancillaries/pax.png',
                title: 'Pasajero flexible',
                type: 'flexible',
                grid: 12,
                text: 'Cambia el nombre de cualquier pasajero de tu reserva hasta 48 horas antes de la salida de tu vuelo',
                price: 40.98,
                added: false,
                options: {
                    available: false,
                    display: false
                },
                views: {
                    pax: false,
                    extra: true
                },
                discount: {
                    available: false,
                    amount: 50
                }
            },
            {
                name: 'flex-dates',
                img: './static/images/ancillaries/dates.png',
                title: 'Fechas de viaje flexibles',
                type: 'flexible',
                grid: 12,
                text: '¿Y si tus planes cambian? Añade flexibilidad a tu viaje y relájate',
                price: 26.45,
                added: false,
                options: {
                    available: false,
                    display: false
                },
                views: {
                    pax: false,
                    extra: true
                },
                discount: {
                    available: false,
                    amount: 50
                }
            },
            {
                name: 'mobile-notifications',
                img: './static/images/ancillaries/notifications.png',
                title: 'Alertas de móvil',
                type: 'complete',
                grid: 12,
                text: 'Sé el primero en enterarte de cualquier cmabio o incidencia en tu vuelocon este servicio de notificaciones',
                price: 1.99,
                added: false,
                options: {
                    available: false,
                    display: false
                },
                views: {
                    pax: false,
                    extra: true
                },
                discount: {
                    available: false,
                    amount: 50
                }
            },
            {
                name: 'wifi',
                img: './static/images/ancillaries/wifi.png',
                title: 'Wifi portátil',
                type: 'complete',
                grid: 12,
                text: 'Disfruta de wifi en todas partes del planeta conectado con hasta 8 dispositivos simultáneamente',
                price: 40.00,
                added: false,
                options: {
                    available: false,
                    display: false
                },
                views: {
                    pax: false,
                    extra: true
                },
                discount: {
                    available: false,
                    amount: 50
                }
            },
            {
                name: 'fast-track',
                img: './static/images/ancillaries/track.png',
                title: 'Fast track',
                type: 'complete',
                grid: 12,
                text: 'Olvídate de las colas y y llega el primero a la puerta de embarque',
                price: 6.98,
                added: false,
                options: {
                    available: false,
                    display: false
                },
                views: {
                    pax: false,
                    extra: true
                },
                discount: {
                    available: false,
                    amount: 50
                }
            }
        ],
        itemsAdded: []
    },
    getters: {
        ancillaries(state) {
            return state.ancillaries
        },
        ancillariesProtect(state) {
            return state.ancillaries.filter(anc => {
                return anc.type === 'protect'
            })
        },
        ancillariesFlexible(state) {
            return state.ancillaries.filter(anc => {
                return anc.type === 'flexible'
            })
        },
        ancillariesComplete(state) {
            return state.ancillaries.filter(anc => {
                return anc.type === 'complete'
            })
        }
    },
    mutations: {
        add(state, entry) {
            entry.added = true;
            let piece = {
                name: entry.name,
                title: entry.title,
                price: entry.price
            }
            state.itemsAdded.push(piece);
            if (state.totalAmount >= 0) {
                state.totalAmount += entry.price;
            }
        },
        remove(state, entry) {
            entry.added = false;
            let removedItem = state.itemsAdded.find(item => {
                return item.name === entry.name;
            });
            state.itemsAdded.shift(removedItem);
            if (state.totalAmount > 0) {
                state.totalAmount -= entry.price;
            }
        },
        load(state) {
            state.loader = !state.loader;
        }
    }
})
