# About the prototype kit

Prototypes Toolkit for eDreams ODIGEO Mobile (a.k.a, **eva-02**). The main goal of this project is to allows UX Team to implement designs iterations of their work in the fastest possible way in a code-based "real" environment as similar as possible to eDreams website. These prototypes can be used to show ideas to people you work with, and to do user research and test.

## Tech Stack: Vue.js + ODF + Webpack + Firebase
Instead of working with OF we decided to run up this project from the beginning using cutting edge technology based on business and team needs. In addition, we thought it could be also a great idea to implement ODIGEO Design Framework (a.k.a, ODF) in order to have... somehow a small "copy" of eDreams website styled with this all new ODF, using Vue.js as the main framework, Webpack as module bundle and Firebase by Google for Hosting, Auth, Cloud Storage, etc. The nature of this project is because of research and exploration, so everyone from the OF team is more than welcome to collaborate resolving issues and developing new ideas and features for the tool and to take ideas from it and run them in their PODs as well if they want.

- Vue.js: https://vuejs.org/
- Webpack: https://webpack.github.io/
- Firebase: https://vuejs.org/

## Community
We have a Slack channel for the Prototype kit, just join us >>> [Slack channel](https://edreamslabs.slack.com/#eva-02)

## Project Structure
Please, notice this is a **work in progress...**.

``` bash
.
├── build/                      # webpack config files
│   └── ...
├── config/
│   ├── index.js                # main project config
│   └── ...
├── dist/
│   ├── index.html              # index.html for production
│   └── static/                 # assets for production
│       └── ...
├── src/
│   ├── main.js                 # app entry file
│   ├── main.scss               # main styles file
│   ├── App.vue                 # main app component
│   ├── components/             # ui components
│   │   ├── views/              # main views components
│   │   │   └── ...
│   │   └── ...
│   ├── scss/                   # app sass files
│   │   ├── partials/
│   │   │   └── ...
│   │   └── ...
│   ├── assets/                 # module assets (processed by webpack)
│   │   └── ...
│   ├── edreamsjs/              # javascript client for eDreams to communicate with backend
│   │   └── ...
│   ├── store/                  # state management pattern + library (Vuex)
│   │   └── index.js
│   ├── traductions/            # traductions for form validations (VeeValidate)
│   │   └── ...
│   └── router/                 # app routes (Vue-router)
│       └── ...
├── static/                     # pure static assets (directly copied)
├── .babelrc                    # babel config
├── .postcssrc.js               # postcss config
├── .eslintrc.js                # eslint config
├── .editorconfig               # editor config
├── index.html                  # index.html template
└── package.json                # build scripts and dependencies
```

## `build/`
This directory holds the actual configurations for both the development server and the production webpack build. Normally you don't need to touch these files unless you want to customize Webpack loaders, in which case you should probably look at build/webpack.base.conf.js.

## `config/index.js`
This is the main configuration file that exposes some of the most common configuration options for the build setup. See API Proxying During Development and Integrating with Backend Framework for more details.

## `src/`
This is where most of your application code will live in. How to structure everything inside this directory is largely up to you; if you are using Vuex, you can consult the [recommendations for Vuex applications](https://vuex.vuejs.org/en/structure.html).

## `static/`
This directory is an escape hatch for static assets that you do not want to process with Webpack. They will be directly copied into the same directory where webpack-built assets are generated.

## `index.html`
This is the **template index.html** for our single page application. During development and builds, Webpack will generate assets, and the URLs for those generated assets will be automatically injected into this template to render the final HTML.

## `package.json`
The NPM package meta file that contains all the build dependencies and build commands.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
